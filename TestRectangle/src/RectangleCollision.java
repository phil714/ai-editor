

/********************************************************
*   													*
*  Fonctions de comparaison de entity de type Rectangle *
*   													*
*********************************************************/


import java.util.Random;
import java.util.Vector;

public class RectangleCollision {
	
	public static void main(String args[])
	{
		Random randX = new Random() ;
		Random randY = new Random() ;
		
		//2 Rectangles pour la comparaison dans RectangleToucher
		entity Rectangle1 = new entity(5,5,0,0,10,10) ;
		entity Rectangle2 = new entity(17,17,0,0,10,10) ;
		
		//Vecteur pour le test al�atoire 
		Vector<entity> Rectangles = new Vector<entity>() ;
		//Vecteur pour le test simple (4 rectangles)
		Vector<entity> SimpleTest = new Vector<entity>() ;
		
		//Ajoute des rectangles al�atoire au vecteur Rectangles
		for(int i = 0 ; i < 10 ; i++)
		{
			Rectangles.add(new entity(randX.nextInt(40),randY.nextInt(40),0,0,randX.nextInt(15),randX.nextInt(15))) ;
		}
		
		
		//Ajoute des rectangles au vecteur SimpleTest 		
		SimpleTest.add(new entity(5,5,0,0,10,10)) ;
		SimpleTest.add(new entity(17,17,0,0,10,10)) ;
		SimpleTest.add(new entity(10,10,0,0,10,10)) ;
		SimpleTest.add(new entity(-8,-8,0,0,10,10)) ;
		
		System.out.println("Es-ce que le rectangle 1 touche au rectangle 2 :\n"+RectangleToucher(Rectangle1,Rectangle2)) ;		
		
		
		System.out.println("\n\n\nAffichage des collisions pour le test simple");
		RectangleToucher2(SimpleTest) ;
		
		System.out.println("\n\n\nAffichage des collisions pour le test al�atoire");
		RectangleToucher2(Rectangles) ;
		
		
		
	}
	
	
	
	/*
		Compare deux classe de type entity (Rectangle) et retourne s'il y a collision entre les deux
		
		Param�tres : Deux entit� de type rectangle
		Retourne : Boolean			
	*/	
		
	static boolean RectangleToucher(entity Rect1, entity Rect2)
	{
		int xg=Rect2.x ;
		int xd=Rect2.x+Rect2.width ;
		int yh=Rect2.y ;
		int yb=Rect2.y+Rect2.height ;		
        
        if (Rect1.x <= xd && Rect1.x >= xg && Rect1.y >= yh && Rect1.y <= yb)
            return true ;
        if (Rect1.x+Rect1.width <= xd && Rect1.x+Rect1.width >= xg && Rect1.y >= yh && Rect1.y <= yb)
            return true ;
        if (Rect1.x+Rect1.width <= xd && Rect1.x+Rect1.width >= xg && Rect1.y+Rect1.height >= yh && Rect1.y+Rect1.height <= yb)
            return true ;
        if (Rect1.x <= xd && Rect1.x >= xg && Rect1.y+Rect1.height >= yh && Rect1.y+Rect1.height <= yb)
            return true ;        
        if (Rect1.x+Rect1.width/2  <= xd && Rect1.x+Rect1.width/2 >= xg && Rect1.y >= yh && Rect1.y <= yb)
            return true ;
        if (Rect1.x+Rect1.width <= xd && Rect1.x+Rect1.width >= xg && Rect1.y+Rect1.height/2 >= yh && Rect1.y+Rect1.height/2 <= yb)
            return true ;
        if (Rect1.x+Rect1.width/2 <= xd && Rect1.x+Rect1.width/2 >= xg && Rect1.y+Rect1.height >= yh && Rect1.y+Rect1.height <= yb)
            return true ;
        if (Rect1.x <= xd && Rect1.x >= xg && Rect1.y+Rect1.height/2 >= yh && Rect1.y+Rect1.height/2 <= yb)     
            return true ;        
        return false ;		
	}
	
	
		/*
		Compare tout les �l�ments du vecteur de entity(Rectangle) entre eux.
		Affiche quels rectangle se touche et ne se touche pas avec leurs indice dans le vecteur
		
		Param�tres : Vector de type entit� (Rectangle)
		Retourne : void			
	*/	
		
	
	
	static void RectangleToucher2(Vector<entity> Tableau)
	{
		int xg ;
		int xd ;
		int yh ;
		int yb ;
		boolean RectangleToucher ;
		
		for(int i = 0 ; i < Tableau.size() ; i++)
		{
			xg = Tableau.elementAt(i).x ;
			xd = Tableau.elementAt(i).x+Tableau.elementAt(i).width ;
			yh = Tableau.elementAt(i).y ;
			yb = Tableau.elementAt(i).y+Tableau.elementAt(i).height ;			
			
			for(int j = 0 ; j < Tableau.size() ; j++)
			{
				RectangleToucher = false ;
				if(i != j)
				{		
			        if (Tableau.elementAt(j).x <= xd && Tableau.elementAt(j).x >= xg && Tableau.elementAt(j).y >= yh && Tableau.elementAt(j).y <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x+Tableau.elementAt(j).width <= xd && Tableau.elementAt(j).x+Tableau.elementAt(j).width >= xg && Tableau.elementAt(j).y >= yh && Tableau.elementAt(i).y <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x+Tableau.elementAt(j).width <= xd && Tableau.elementAt(j).x+Tableau.elementAt(j).width >= xg && Tableau.elementAt(j).y+Tableau.elementAt(i).height >= yh && Tableau.elementAt(i).y+Tableau.elementAt(i).height <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x <= xd && Tableau.elementAt(j).x >= xg && Tableau.elementAt(j).y+Tableau.elementAt(j).height >= yh && Tableau.elementAt(j).y+Tableau.elementAt(i).height <= yb)
			            RectangleToucher = true ;        
			        if (Tableau.elementAt(j).x+Tableau.elementAt(j).width/2  <= xd && Tableau.elementAt(j).x+Tableau.elementAt(j).width/2 >= xg && Tableau.elementAt(i).y >= yh && Tableau.elementAt(i).y <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x+Tableau.elementAt(j).width <= xd && Tableau.elementAt(j).x+Tableau.elementAt(j).width >= xg && Tableau.elementAt(i).y+Tableau.elementAt(i).height/2 >= yh && Tableau.elementAt(i).y+Tableau.elementAt(i).height/2 <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x+Tableau.elementAt(j).width/2 <= xd && Tableau.elementAt(j).x+Tableau.elementAt(j).width/2 >= xg && Tableau.elementAt(i).y+Tableau.elementAt(i).height >= yh && Tableau.elementAt(i).y+Tableau.elementAt(i).height <= yb)
			            RectangleToucher = true ;
			        if (Tableau.elementAt(j).x <= xd && Tableau.elementAt(j).x >= xg && Tableau.elementAt(j).y+Tableau.elementAt(j).height/2 >= yh && Tableau.elementAt(i).y+Tableau.elementAt(i).height/2 <= yb)     
			            RectangleToucher = true ;        
			        
			        if(RectangleToucher)			        
			        	System.out.println("Le rectangle "+i+" touche au rectangle"+j) ;			        
			        else			        
			        	System.out.println("Le rectangle "+i+" touche pas au rectangle"+j) ;
			          
				}
			}
		}
	}
}


class entity
{
	int x ;
	int y;
	float vitesseX, vitesseY ;
	int width, height ;	
	
	
	entity(int x, int y, float vitesseX, float vitesseY, int width, int height)
	{
		this.x = x ;
		this.y = y ;
		this.vitesseX = vitesseX ;
		this.vitesseY = vitesseY ;
		this.width = width ;
		this.height = height ;
	}
}

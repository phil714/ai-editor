import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JTextField;


public class DialogNewType extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Interface inter ;
	private JTextField textField;
	JComboBox comboBox ;
	JPanel buttonPane ;
	JButton okButton ;
	JButton cancelButton ;

	/**
	 * Create the dialog.
	 */
	public DialogNewType(Interface inter, String title, boolean modal) {
		super(inter,title,modal) ;
		this.inter = inter ;
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JList list = new JList();
		list.setBounds(10, 202, 1, 1);
		contentPanel.add(list);
		
		comboBox = new JComboBox(inter.GetTypes());
		comboBox.setBounds(10, 11, 162, 217);
		contentPanel.add(comboBox);
		
		textField = new JTextField();
		textField.setBounds(202, 109, 190, 20);
		contentPanel.add(textField);
		textField.setColumns(10);
		{
			buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Annuler");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		Ecouteur ec = new Ecouteur() ;
		comboBox.addActionListener(ec) ;
		textField.addActionListener(ec) ;
		
		
		
		
		
	}
	
	class Ecouteur extends WindowAdapter implements ActionListener 
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == okButton)
			{
				if(textField.getText() != null && textField.getText() != "")
				{	
					
					//Il faut v�rifier si la classe n'existe pas d�j�
					// TO DO
					
					inter.typeName = textField.getText() ;
					inter.nouveauType = true ;
					dispose() ;
				}
				else
				{
					inter.typeName = (String) comboBox.getSelectedItem() ;	
					dispose() ;
				}				
			}
			if(e.getSource() == cancelButton)
			{
				inter.canceledCreation = true ;
				dispose() ;
			}
			
			
			
		}
		public void windowClosed(WindowEvent e) {
	        inter.canceledCreation = true ;
	    }
	}
}

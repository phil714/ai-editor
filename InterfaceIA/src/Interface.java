import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JToggleButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JMenuItem;
import javax.swing.JEditorPane;
import javax.swing.JComboBox;

//import org.apache.commons.io.filefilter.DirectoryFileFilter;


public class Interface extends JFrame {	
	boolean BoutonOn = false ; //Est � true lorsque les boutons avancer sont afficher
	Vector<Pouvoir> Abilities = new Vector<Pouvoir>() ;
	Interface frame ;	
	JLabel []AllPower = new JLabel[100] ;
	JTextField []Texts = new JTextField[100] ;
	
	//Boutons
	private JButton btnSave,btnPouvoir,btnImage,btnLoad,btnQuitter,btnBibite,btnLoadPower,btnNewButton, btnNouveauType   ;
	private JPanel panel1 ;
		
	//Variables pour Save/Open things
	private JFileChooser SavingMode ;	
	private JFileChooser LoadingMode ;	
	int saveStatus ;
	int loadStatus ;
	
	//Nom de la bibite courante et de sa classe
	String className = "Red_Slime" ;
	String typeName = "Slime" ;
	
	
	//Dialog New 
	private JOptionPane newDialog ;
	
	
	
	//Variables pour le menu
	JMenuBar MenuBar ; //Menu au complet
	
	JMenu menuFile ;//Menu File
	JMenuItem menuItemNew ;
	JMenuItem menuItemLoad ;
	
	
	//Ecouteur
	Ecouteur ec ;
	private JEditorPane powerEditor;
	private JButton btnSavepower;
	
	//Pour v�rifier la cr�ation d'un type
	public boolean nouveauType = false ;
	public String toVerify ;
	public boolean canceledCreation ;
	private JComboBox comboBoxType;
	private JComboBox comboBoxClasse;
	private ComboBoxModel cbModele ;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface() {
		ec = new Ecouteur() ;				
		LoadStartingInterface() ;			
		
		/********************************************
		 ********************************************
		 *Interface qui ajoute des boutons lorsqu'on ouvre*****
		 *une bibite ou qu'on en fait une nouvelle***
		 ******************************************** 
		 ********************************************/
		//BoutonPouvoir
		btnPouvoir = new JButton("Pouvoirs");
		btnPouvoir.setBounds(10, 104, 146, 37);
		panel1.add(btnPouvoir);
		

		//Bouton Save
		btnSave = new JButton("");
		btnSave.setBounds(367, 7, 43, 37);
		panel1.add(btnSave);
		btnSave.setIcon(new ImageIcon("save.gif"));

		//Bouton Ajouter une Image
		btnImage = new JButton("Ajouter une Image");
		btnImage.setBounds(10, 288, 146, 37);
		panel1.add(btnImage);

		btnNouveauType = new JButton("Nouveau type");
		btnNouveauType.setBounds(10, 7, 146, 37);
		panel1.add(btnNouveauType);

		JLabel lbl1 = new JLabel("Type : ");
		lbl1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl1.setBounds(166, 22, 46, 14);
		panel1.add(lbl1);

		JLabel lbl2 = new JLabel("Classe : ");
		lbl2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl2.setBounds(166, 67, 46, 14);
		panel1.add(lbl2);

		JLabel lblType = new JLabel("");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblType.setBounds(212, 22, 46, 14);
		panel1.add(lblType);

		JLabel labelClasse = new JLabel("");
		labelClasse.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelClasse.setBounds(212, 67, 46, 14);
		panel1.add(labelClasse);

		comboBoxType = new JComboBox<String>(GetTypes());
		comboBoxType.setBounds(212, 21, 89, 17);
		panel1.add(comboBoxType);
		typeName = (String) comboBoxType.getSelectedItem() ;

		comboBoxClasse = new JComboBox();
		comboBoxClasse.setBounds(212, 66, 89, 17);
		panel1.add(comboBoxClasse);
		
		
		//Boutons inactifs au d�but du programme
		btnImage.setVisible(false);
		btnPouvoir.setVisible(false);	
		btnSave.setVisible(false);

		//Ajouter les Labels des Powers
		AddPowerLabels(10, 100) ;

		
		//Ajout des boutons � l'�couteur ec		
		Component[] tab = panel1.getComponents() ;		
		for(Component c : tab)
		{
			if(c instanceof JButton)
			{
				((JButton)c).addActionListener(ec) ;
			}
			if(c instanceof JComboBox)
			{				
				((JComboBox)c).addActionListener(ec) ;
			}
		}

		
		//Ajout des actionListener sur tout les MenuItem de Menu		
		menuItemNew.addActionListener(ec) ;
		menuItemLoad.addActionListener(ec) ;
	}
	//Fin du constructeur
	
	
	/*********************************************************************************************************************
	*********************FONCTIONS/M�THODES EN LIENS AVEC LE GUI**********************************************************
	**********************************************************************************************************************/
	
	/*Fonction qui rend visible les boutons 
	*relatif � la cr�ation d'une IA
	*Param�tres : Aucun 
	*Return : Fuckall 
	*/
	void AjouterBouton()
	{
		if(!BoutonOn)
		{
			BoutonOn = true ;			
			btnSave.setVisible(true);
			btnPouvoir.setVisible(true);
			btnImage.setVisible(true);
		}
	}
	
	
	/*Fonction qui cr�er les Boutons Quitter,LoadIA(le folder) et NouvelleIA(bibitte)
	*Param�tres : Aucun
	*Return : Fuckall
	*/
	void LoadStartingInterface()
	{
		//Pouvoir de test
		Abilities.add(new Pouvoir("Fart")) ;
		Abilities.add(new Pouvoir("Punch")) ;
		Abilities.add(new Pouvoir("Kick")) ;
		
		//Save/Open File things
		SavingMode = new JFileChooser() ;
		LoadingMode = new JFileChooser() ;
		
		
		//Fen�tre		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 800);		
		panel1 = new JPanel();
		panel1.setBounds(10, 11, 306, 740);
		getContentPane().add(panel1);
		panel1.setLayout(null);
		
		
		/******************************
		 ****************************** 
		 ******Interface de base*******
		 ****************************** 
		 ******************************/
		
		
		//Menus
		MenuBar = new JMenuBar() ;
		setJMenuBar(MenuBar);		
		menuFile = new JMenu("File") ;
		MenuBar.add(menuFile) ;
		
		menuItemNew = new JMenuItem("New...");
		menuFile.add(menuItemNew);
		
		menuItemLoad = new JMenuItem("Load") ;
		menuFile.add(menuItemLoad) ;
		

		//Bouton Nouvelle bibitte
		btnBibite = new JButton("Nouvelle bibitte");		
		btnBibite.setBounds(10, 56, 146, 37);
		panel1.add(btnBibite);
		
		
		//Bouton Load
		btnLoad = new JButton("");
		btnLoad.setBounds(314, 7, 43, 37);
		panel1.add(btnLoad);
		btnLoad.setIcon(new ImageIcon(Interface.class.getResource("/com/sun/java/swing/plaf/windows/icons/TreeClosed.gif")));
		
		
		//Bouton Quitter
		btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(10, 692, 146, 37);
		panel1.add(btnQuitter);
		
		btnNewButton = new JButton("Nouveau pouvoir...");
		btnNewButton.setBounds(1028, 11, 146, 37);
		panel1.add(btnNewButton);
		
		powerEditor = new JEditorPane();
		powerEditor.setText("\u00C9crire votre code ici...");
		powerEditor.setBounds(665, 11, 352, 305);
		panel1.add(powerEditor);
		
		btnSavepower = new JButton("Save");
		btnSavepower.setBounds(928, 327, 89, 23);
		panel1.add(btnSavepower);
		
		btnLoadPower = new JButton("Load");
		btnLoadPower.setBounds(665, 327, 89, 23);
		panel1.add(btnLoadPower);
		

		/******************************
		 ****************************** 
		 *Fin de l'Interface de base***
		 ****************************** 
		 ******************************/
	}
	
	
	/*Fonction qui ajoute autant de Label et de TextField qu'il y a
	*de String dans le vecteur Abilities 
	*Param�tres : Position X,Y de l'endroit o� ont veut placer la liste dans le GUI
	*Return : Fuckall
	*/
	void AddPowerLabels(int startingX, int startingY)
	{
		//Ajoute dans une boucle tout les powers da
		for(int i = 0 ; i < Abilities.size(); i++)
		{
			System.out.println(Abilities.elementAt(i).getNom());
			
			AllPower[i] = new JLabel(Abilities.elementAt(i).getNom()+"") ;						
			AllPower[i].setBounds(startingX, startingY+i*40, 96, 37);
			AllPower[i].setVisible(false);
			panel1.add(AllPower[i]);
			Texts[i] = new JTextField();
			Texts[i].setBounds(startingX+60, startingY+i*40, 96, 37);
			panel1.add(Texts[i]);
			Texts[i].setColumns(10);
			Texts[i].setVisible(false);
		}		
	}
	/*********************************************************************************************************************
	*********************�COUTEURS KING GOD QUI FAIT TOUTE DANS VIE*******************************************************
	**********************************************************************************************************************/
	class Ecouteur implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnSave)
			{
				//Ouvre l'interface pour saver une IA
				Saving() ;
			}
			if(e.getSource() == btnQuitter)
			{
				System.exit(0) ;
			}
			if(e.getSource() == btnLoad || e.getSource() == menuItemLoad )
			{
				loadStatus = LoadingMode.showOpenDialog(null) ;
				if(loadStatus == JFileChooser.APPROVE_OPTION)
				{
					AjouterBouton() ;
				}	
				
			}
			if(e.getSource() == btnBibite || e.getSource() == menuItemNew)
			{					
				String temp = null ;				
				
				temp = PopupType(GetTypes()) ;
				if(temp != null && temp.length() != 0)
				{
					temp = PopupNewBibite(temp) ;
					if(temp != null && temp.length() != 0)
					{
						CreateNewClass(temp) ;
						AjouterBouton() ;
					}
					else
						JOptionPane.showMessageDialog(frame, "Impossible de cr�er la classe", "Erreur", JOptionPane.ERROR_MESSAGE);	
				}
				else					
					JOptionPane.showMessageDialog(frame, "Impossible de cr�er le type", "Erreur", JOptionPane.ERROR_MESSAGE);
				
			
				
			}
			if(e.getSource() == btnNouveauType)
			{
					String s = (String)JOptionPane.showInputDialog(
					                    frame,
					                    "Veuillez entrez le nom de votre nouveau type",
					                    "Nouveau type",
					                    JOptionPane.PLAIN_MESSAGE,
					                    new ImageIcon ("MesImages\\PetSlime1.png"),
					                    null,
					                    "Slime");
					
					if(s != null || s != "")
					{
						boolean Existe = false ;
						
						//V�rifier si le Type existe d�j� 
						// TO DO 
						
						
						if(!Existe)
						{
							CreateTypeDir(s) ;
							typeName = s ;
							
						}						
					}
					else
						JOptionPane.showMessageDialog(frame, "Impossible de cr�er le type", "Erreur", JOptionPane.ERROR_MESSAGE);

			}
			
			if(e.getSource() == btnImage)
			{
				loadStatus = LoadingMode.showOpenDialog(null) ;
				if(loadStatus == JFileChooser.APPROVE_OPTION)
				{
					//Loader un sprite pour l'IA
				}	
			}
			if(e.getSource() == btnPouvoir)
			{
				for(int i = 0 ; i < Abilities.size(); i++)
				{
					System.out.println(Abilities.elementAt(i).getNom());
					AllPower[i].setVisible(true);						
					Texts[i].setVisible(true);
				}
				AllPower[1] = new JLabel(Abilities.elementAt(1).getNom()+"") ;
			}	
			if(e.getSource() == btnSavepower)
			{
				SavePower() ;			
			}
			if(e.getSource() == btnLoadPower)
			{
				LoadPower() ;
			}
			if(e.getSource() == comboBoxType)
			{
				System.out.println("Test");
				typeName = (String) comboBoxType.getSelectedItem() ;
				System.out.println("Type name : "+typeName);
				//cbModele = new ComboBoxModel(GetClasses()) ;
				comboBoxClasse.removeAllItems() ;
				String [] temp = GetClasses() ;
				for(int i = 0 ; i < temp.length ;i++)
				{
					comboBoxClasse.addItem(temp[i]) ;
				}				
			}
			if(e.getSource() == comboBoxClasse)
			{
				className = (String) comboBoxClasse.getSelectedItem() ;
			}			
		}
	}
	
	/*Fonction qui ouvre un pop-up pour saver(Save as...) et qui remplace
	*dans le nom de la classe par le nom entr� dans le nouveau fichier
	*de String dans le vecteur Abilities 
	*Param�tres : Aucun
	*Return : Fuckall
	*/	
	public void Saving() 
	{
		
		SavingMode.setSelectedFile(new File(className+".jar"));
		saveStatus = SavingMode.showSaveDialog(null) ;
		if(saveStatus == JFileChooser.APPROVE_OPTION)
		{	
			String temp = EnleveJAR(SavingMode.getSelectedFile().getName()+"") ;
			System.out.println(temp);
			String newClass = SavingMode.getSelectedFile().getName() ;
			System.out.println("La Nouvelle Classe : "+newClass);			
			System.out.println("Option Approuved");
			RemplacerNomClasse(className,newClass,1) ;			
		}
	}
	
	
	/*Fonction qui ouvre un pop-up qui permet � l'utilisateur de rentr� un nom 
	*de classe de d�part
	*Param�tres : Aucun
	*Return : Nom de la classe
	*/
	String PopupNewBibite(String nomType)
	{	
		if(nomType != null || nomType != "")
		{
			String s = (String)JOptionPane.showInputDialog(
			                    frame,
			                    "Veuillez entrez le nom de votre Bibite (classe)",
			                    "Nouvelle Bibite",
			                    JOptionPane.PLAIN_MESSAGE,
			                    new ImageIcon ("MesImages\\PetSlime1.png"),
			                    null,
			                    "Red_Slime");
			return s ;	
		}
		else
			return null ;
	}
	
	/*Fonction qui remplace le nom de la classe
	*de la classe courant par une nouveau nom 
	*Param�tres : String : Nom de la classe courante
	*, 			  String : Nouveau nom, 
	*			  int : Endroit o� on prend l'info ; 1 = JFileChooser, 2 = JTextField
	*Return : Fuckall
	*/	
	void RemplacerNomClasse(String oldClassName, String newClassName, int endroitInfo) 
	{
		//Read le file qui est cr�er au d�but du programme avec le JDialog
		File oldFile = new File("MesIAs\\"+className+".jar") ;//New Old File	
		
		FileWriter fwriter;
		try {
			System.out.println(SavingMode.getCurrentDirectory());
			
			//Ouvre le nouveau fichier qui sera ou la personne l'a enregistr� avec le nouveau nom qu'elle lui a donn�
			fwriter = new FileWriter(SavingMode.getCurrentDirectory()+"\\"+newClassName+".jar");
			
			PrintWriter outputFile = new PrintWriter(fwriter) ;			
			
			//Scanner de l'ancien fichier
			Scanner inputFile = new Scanner(oldFile) ;
			
			//�crit la premi�re ligne de l'ancier fichier dans le nouveau
			
			
			String line = "", oldtext = "";
			do
			{
				line = inputFile.nextLine() ;
				oldtext+= line + "\r\n" ;
			}
			while(inputFile.hasNextLine()) ;
			
			System.out.println(oldtext);
			
			String newText =  oldtext.replaceAll(oldClassName,newClassName) ;
			fwriter.write(newText)	;
			

			inputFile.close() ;
			outputFile.close() ;
			className = newClassName ;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	
	/*Fonction qui �crit une nouvelle classe dans un fichier
	*avec une structure de base et change le nom de la classe Courante
	*Param�tres : String : Nom de la nouvelle classe	*		 
	*Return : Fuckall
	*/	
	void CreateNewClass(String Name)
	{
		if(Name != null || Name != "")
		{
			className = Name ;
			comboBoxClasse = new JComboBox(GetClasses());
			FileWriter fwriter;
			char b1 = (char)123 ;
			char b2 = (char)125 ;
			
			String BasicStructure = "class "+className+"\n"+b1+"\n\t"+className+"()\n\t"+b1+"\n\n\t"+b2+"\n"+b2 ;
			System.out.println(BasicStructure);
			
			try {
				
				//Cr�ation de la structure de dossier de la nouvelle classe
				File newDir = new File("MesIAs\\"+typeName+"\\"+className) ;
				if (!newDir.exists())			{
					newDir.mkdir() ;
				}
				newDir = new File("MesIAs\\"+typeName+"\\"+className+"\\MesPowers") ;
				if (!newDir.exists())			{
					newDir.mkdir() ;
				}
				
				//Cr�ation du .jar
				fwriter = new FileWriter("MesIAs\\"+typeName+"\\"+className+"\\"+className+".jar");
				fwriter.write(BasicStructure)	;
				fwriter.close() ;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	void CreateNewPower(String powerName, String powerRanger)
	{

	}
	
	
	/*Fonction qui enregistre le pouvoir courant dans un fichier	*
	*Param�tres : Aucun	*		 
	*Return : Fuckall
	*/	
	void SavePower() 
	{
		String newFunction =  powerEditor.getText() ;
		System.out.println(newFunction);
		saveStatus = SavingMode.showSaveDialog(null) ;
		if(saveStatus == JFileChooser.APPROVE_OPTION)
		{
			String temp = EnleveJAR(SavingMode.getSelectedFile().getName()+"") ;
			try {
				FileWriter fwriter = new FileWriter(SavingMode.getCurrentDirectory()+"\\"+temp+".jar") ;
				new PrintWriter(fwriter) ;
				fwriter.write(newFunction)	;
				fwriter.close() ;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
		}
	}
	
	
	/*Fonction qui load un pouvoir (n'importe quoi en faite)
	*dans l'�diteur de texte	*
	*Param�tres : Aucun	*		 
	*Return : Fuckall
	*/	
	void LoadPower()
	{
		String line = "", textToEdit = "";
		loadStatus = LoadingMode.showOpenDialog(null) ;
		if(loadStatus == JFileChooser.APPROVE_OPTION)
		{	
			System.out.println(LoadingMode.getSelectedFile());
			try {
				File powerToEdit = new File(LoadingMode.getSelectedFile()+"") ;
				Scanner inputFile = new Scanner(powerToEdit) ;
				
				do
				{
					line = inputFile.nextLine() ;
					textToEdit+= line + "\r\n" ;
				}
				while(inputFile.hasNextLine()) ;					
			} catch (FileNotFoundException e) {				
				//e.printStackTrace();
			}
			powerEditor.setText(textToEdit) ;			
		}
	}
	
	
	/*Fonction qui enl�ve les .jar � la fin d'un String
	**Param�tres : Le String qui doit �tre modifi�	*		 
	*Return : La chaine modifi�e
	*/	
	String EnleveJAR(String toCrop)
	{
		for(int i = 0 ; i < toCrop.length() ; i++)
			if(toCrop.charAt(i) == '.')
			{
				toCrop = toCrop.substring(0,i-1) ;
			}		
		return toCrop ;
	}
	
	String[] GetTypes()
	{	
		int i = 0 ;
		String[] typeName ;
		File directory = new File("MesIAs");
		File[] subdirs = directory.listFiles();
		typeName = new String[subdirs.length] ;
		for (File dir : subdirs) {
			System.out.println("Directory: " + dir.getName());
			if(dir.getName() != "MesPowers")
				typeName[i] = dir.getName() ;
			else
				i-- ;
			i++ ;
		}
		return typeName;			
	}
	String[] GetClasses()
	{	
		int i = 0 ;
		String[] classNames ;
		File directory = new File("MesIAs\\"+typeName);
		File[] subdirs = directory.listFiles();
		classNames = new String[subdirs.length] ;
		for (File dir : subdirs) {
			System.out.println("Directory: " + dir.getName());
			if(dir.getName() != "MesPowers")
				classNames[i] = dir.getName() ;
			else
				i-- ;
			i++ ;
		}
		System.out.println("Class name :"+classNames.length);
		return classNames;			
	}
	
	
	String PopupType(String[] typeNames)
	{

		//IL FAUT FAIRE UN JDIALOGS
		DialogNewType monDialog = new DialogNewType(Interface.this,"Fen�tre",true) ;
		monDialog.setVisible(true) ;
		
		if(canceledCreation)
		{
			canceledCreation = false ;			
			return null ;
		}
		else
		{			
			if(nouveauType)
			{			
				nouveauType = false ;				
				CreateTypeDir(typeName) ;
				comboBoxType.setSelectedItem(typeName);
				return typeName ;
			}						
			else
			{				
				comboBoxType.setSelectedItem(typeName);
				return typeName ;			
			}			
		}
	}	
	void CreateTypeDir(String dirName)
	{
		File newDir = new File("MesIAs\\"+dirName) ;
		if (!newDir.exists() && newDir.getName() != "MesPowers")
		{
			newDir.mkdir() ;
		}
		else
			System.out.println("Unable to create directory"); ;
	}	
}
